package com.example.sleuth;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@RestController("/api")
@Slf4j
public class SleuthApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(SleuthApplication.class, args);
	}
	
	@GetMapping("/name")
	public String hello() {
		MDC.put("transaction.id", "23123123123");
        MDC.put("transaction.owner", "SLEUTH");
        //log.transfer(0);
//        MDC.clear();
		log.info("In");
		return "Farith";
	}

}

